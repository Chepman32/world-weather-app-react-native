import { View, Text, StyleSheet } from 'react-native'
import React, { FC } from 'react'
import { SmallWeather } from '../components/SmallWeather'
interface ICitiesList {
    cities?: string[]
}
export const CitiesList: FC<ICitiesList> = ({cities}) => {
    const items = [
        "New York", "Moscow", "Berlin", "Singapore", "New York", "Moscow", "Berlin", "Singapore", "New York", "Moscow", "Berlin", "Singapore",
    ]
  return (
    <View style={styles.container} >
      {
        items ? items.map((c: string) => <SmallWeather key={c} city={c}/>) : <Text>Loading...</Text>
      }
    </View>
  )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        flexDirection: "row",
    flexWrap: "wrap",
        justifyContent: "space-around",
        alignItems: "stretch",
    }
})