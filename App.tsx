import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, Button, PermissionsAndroid, Platform} from 'react-native';
import Video from 'react-native-video';
import { SnapCarousel } from './components/SnapCarousel';
import { WeatherAPI } from './screens/WeatherAPI.com';

const App = () => {
  const [granted, setGranted] = useState(false)
  const requestLocationPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Geolocation Permission',
        message: 'Can we access your location?',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    console.log('granted', granted);
    if (granted === 'granted') {
      console.log('You can use Geolocation');
      return true;
    } else {
      console.log('You cannot use Geolocation');
      return false;
    }
  } catch (err) {
    return false;
  }
};
  return (
    <SnapCarousel/>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#573"
  },
  content: {
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    zIndex: 3,
  },
  title: {
    fontWeight: '500',
  },
  city: {
    marginVertical: 10,
    fontSize: 30,
    fontWeight: "500"
  },
  temp: {
    marginVertical: 10,
    fontSize: 30,
    fontWeight: "500"
  },
  icon: {
    width: 60,
    height: 60,
  },
  videoBackground: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1
  }
});
export default App;