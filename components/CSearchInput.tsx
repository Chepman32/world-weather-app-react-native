import { View, Text } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios'

export const CSearchInput = () => {
    const [cities, setCities] = useState([])
    useEffect(() => {
        getCities()
    }, [])
    const getCities = async () => {
        const res = await axios.get("https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json")
        const { data } = res
        console.log(data)
        setCities(data)
    }
  return (
    <View>
      <Text>CSearchInput</Text>
    </View>
  )
}