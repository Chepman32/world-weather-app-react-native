import React, { useState, useEffect } from 'react';
import {
  Text, 
  View,
  SafeAreaView } from 'react-native';

import Carousel from 'react-native-snap-carousel';
import { WeatherAPI } from '../screens/WeatherAPI.com';
export const SnapCarousel = () => {
  const [items, setItems] = React.useState([
    "", "New York", "Moscow", "Berlin", "Singapore"
  ])
  const [activeIndex, setActiveIndex] = React.useState(0)

    const _renderItem = ({item,index}) => {
        return (
          <View style={{flex: 1}}>
            <WeatherAPI city={item}/>
          </View>

        )
    }
    let carousel = React.useRef()

    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{ flex: 1 }}>
            <Carousel
            activeAnimationType="timing"
            slideStyle={{opacity: 1}}
              layout={"tinder"}
              ref={ref => carousel = ref}
              data={items}
              sliderWidth={500}
              itemWidth={500}
              renderItem={_renderItem}
              onSnapToItem = { index => setActiveIndex(index) } />
        </View>
      </SafeAreaView>
    );
}

