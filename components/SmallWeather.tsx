'use strict';

import React, { FC, useEffect, useState } from 'react';
import { StyleSheet, Text, View, Alert, Image, TouchableOpacity } from 'react-native';
import Geolocation, { GeolocationResponse } from '@react-native-community/geolocation';
import axios from "axios"
import Video from 'react-native-video';
import { files, makeUrl } from "../files"
interface ISmallWeather {
  city?: string
  position?: any
}
export const SmallWeather: FC<ISmallWeather> = ({ city }) => {
  const [loading, setLoading] = useState(true);
  const [weather, setWeather] = useState<any>()
  const [position, setPosition] = useState<GeolocationResponse | null>(null);
  const [url, setUrl] = useState<number | {
    uri?: string | undefined;
    headers?: {
        [key: string]: string;
    } | undefined;
    type?: string | undefined;
}>(0)
  useEffect(() => {
    !weather && makeAPICall()
  }, [weather])
  useEffect(() => {
    !position && getCurrentPosition()
    !city && position && !weather && makeAPICall()
  }, [position, weather])
  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      (pos) => {
        setPosition(pos);
        
      },
      (error) => Alert.alert('GetCurrentPosition Error', JSON.stringify(error)),
      { enableHighAccuracy: true }
    );
    
  };
  const makeAPICall = async () => {
    const query = city ? city : `${position?.coords.latitude},${position?.coords.longitude}`
    try {
      setLoading(true)
      let key = "ad6aefbe1fb04377a54154651200811"
    const res = await axios.get(`https://api.weatherapi.com/v1/current.json?key=${key}&q=${query}`)
    const current = res.data
    setWeather(current)
    console.log("API called")
    
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false)
  }
  }
  return (
    <View style={styles.container} >
      <View style={styles.content}>
      <Text style={styles.city} >
        {
          weather ? `${city || weather?.location.name}` : ""
        }
      </Text>
      <Text>{
        !city && position && "Current city"
        } </Text>
      
      <Text style={styles.temp} >
        {
          weather ? weather?.current.temp_c : ""
        }
      </Text>
      <TouchableOpacity onPress={() => console.log(`${weather.current.condition.text}`)}>
        </TouchableOpacity>
      </View>
      {
          weather ? <Video style={styles.videoBackground} source={makeUrl(weather)} resizeMode="cover" repeat muted /> : <Text>Loading...</Text>
        }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "25%",
    height: "17%",
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    zIndex: 3,
  },
  title: {
    fontWeight: '500',
  },
  city: {
    marginVertical: 10,
    fontSize: 30,
    fontWeight: "500"
  },
  temp: {
    marginVertical: 10,
    fontSize: 30,
    fontWeight: "500"
  },
  icon: {
    width: 60,
    height: 60,
  },
  videoBackground: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1,
    
  }
});